#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields

class TextModule(ModelSQL, ModelView):
    'Text Module'
    _name = "text_module.text_module"
    _description = __doc__

    def __init__(self):
        super(TextModule, self).__init__()
        self._order.insert(0, ('sequence', 'ASC'))

    name = fields.Char('Name', required=True, translate=True, loading='lazy',
        select=1)
    text = fields.Text('Text', required=True, translate=True, loading='lazy')
    active = fields.Boolean('Active', select=1)
    sequence = fields.Integer('Sequence')

    def default_active(self):
        return True

    def default_sequence(self):
        return 10

TextModule()
